<?php

include __DIR__ . "/vendor/autoload.php";
include __DIR__ . "/aplicacion/librerias/autoload.php";
include __DIR__ . "/aplicacion/controladores/autoload.php";
// Desarrollo
include __DIR__ . "/desarrollo/index.php";


define('HTTP_DEFAULT_CONTENT_TYPE', 'application/json');
//use modelos\Informes\Documentacion as Documentacion;


$app = new \Slim\App();

$app->get('/{clase}', 'metodoGet');
$app->get('/{clase}/{metodo}', 'metodoGet');
$app->get('/{clase}/{metodo}/[{params:.*}]', 'metodoGet');

$app->post('/{clase}', 'metodoPost');
$app->post('/{clase}/{metodo}', 'metodoPost');

$app->put('/{clase}', 'metodoPut');
$app->put('/{clase}/{metodo}', 'metodoPut');
$app->put('/{clase}/{metodo}/[{params:.*}]', 'metodoPut');

$app->delete('/{clase}/{metodo}/[{params:.*}]', 'metodoDelete');
$app->run();

function metodoGet($request, $response, $args) {
    $contentType = 'application/json';
    if (class_exists($args['clase'])) {
        $objeto = new $args['clase'] ();
        if (!isset($args['metodo'])) {
            $args['metodo'] = null;
        }
        if (!isset($args['params'])) {
            $args['params'] = null;
        }
        return codigo200($response, $objeto->_get($args['metodo'], $args['params']));
    }
    return codigo404($response);
}

function metodoPost($request, $response, $args) {
    $contentType = 'application/json';
    if (class_exists($args['clase'])) {
        $objeto = new $args['clase'] ();
        return codigo200($response, $objeto->_post($args['metodo'], $request->getParams()));
    }
    return codigo404($response);
}

function metodoPut($request, $response, $args) {
    $contentType = 'application/json';
    if (class_exists($args['clase'])) {
        $objeto = new $args['clase'] ();
        return codigo200($response, $objeto->_put($args['metodo'], $request->getParams()));
    }
    return codigo404($response);
}

//
function metodoDelete($request, $response, $args) {
    $contentType = 'application/json';
    if (class_exists($args['clase'])) {
        $objeto = new $args['clase'] ();
        return codigo204($response, $objeto->_delete($args['metodo'], $args['params']));
    }
    return codigo404($response);
}

function codigo200($response, $contenido) {
    return $response->withStatus(200)
                    ->write(json_encode(array('codigo' => 200, 'contenido' => $contenido)))
                    ->withHeader('Content-Type', 'application/json');
}

function codigo404($response) {
    return $response->withStatus(404)
                    ->write(json_encode(array('codigo' => 404, 'mensaje' => 'No se ha encontrado el recurso')))
                    ->withHeader('Content-Type', 'application/json');
}

function codigo204($response) {
    return $response->withStatus(204)
                    ->withHeader('Content-Type', 'application/json');
}
