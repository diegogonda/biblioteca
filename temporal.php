<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title>ARchivo temporal mientras no desarrollo algo mas chulo</title>
        <link type="text/css" rel="stylesheet" href="anadir/bower_components/bootstrap/dist/css/bootstrap.min.css"/>
        <link type="text/css" rel="stylesheet" href="anadir/css/estilos.css"/>
    </head>

    <body>
        <form method="get"  enctype="application/json" id="form-insertar">
            <div>
                <label for="input-url">URL</label>
                <input type="text" id="input-url" name="input-url" />
            </div>
            <div>
                <label for="input-titulo">T&iacute;tulo</label>
                <input type="text" id="input-titulo" name="input-titulo" />
            </div>
            <div>
                <label for="input-contenido">Contenido</label>
                <input type="text" id="input-contenido" name="input-contenido" />
            </div>
            <div>
                <button class="btn btn-danger" id="submit-form">
                    Enviar!
                </button>
            </div>
        </form>
        <script type="text/javascript" src="anadir/bower_components/jquery/dist/jquery.min.js"></script>
        <script type="text/javascript" src="anadir/js/recursos/patrones.js"></script>
        <script type="text/javascript" src="anadir/js/escuchadores.js"></script>
        <script type="text/javascript" src="anadir/js/index.js"></script>
    </body>

</html> 