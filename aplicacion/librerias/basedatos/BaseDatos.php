<?php

/**
 * Description of BaseDatos
 *
 * @author diego.gonda
 */
class BaseDatos {

    private $servidor;
    private $usuario;
    private $contrasena;
    private $base_datos;
    private $mysqli;
    private $consulta;
    private $query;

    public function __construct() {
        $this->servidor = "localhost";
        $this->usuario = "diego";
        $this->contrasena = "abc123..";
        $this->base_datos = "informes";
    }

//    public function insertarCache($array) {
//        $this->consulta = 'INSERT IGNORE INTO restful.`cache` (clave, valor) VALUES ("' . $array['clave'] . '", "' . $array['valor'] . '");';
//        $this->lanzarQuery();
//    }

    public function value_encode($valor) {
        return json_encode($valor, JSON_UNESCAPED_UNICODE);
    }

    public function value_decode($valor) {
        return json_decode($valor);
    }

    public function actualizarCache($array) {
        $this->consulta = 'UPDATE restful.cache SET valor="' . $array ['valor'] . '" WHERE clave="' . $array['clave'] . '";';
        if ($this->lanzarQuery()) {
            return $this->query;
        }
        return [];
    }

    public function lanzarQuery($consulta = null) {
        if (isset($consulta) && !is_null($consulta)) {
            $this->consulta = $consulta;
        }
        $this->conectar();
        $this->query = $this->mysqli->query($this->consulta);
        if (!$this->query) {
            echo "Falló la creación de la tabla: (" . $this->mysqli->errno . ") " . $this->mysqli->error;
            return false;
        }
        return true;
        $this->desconectar();
    }

    public function ejecutarQuery($consulta) {
        if ($this->lanzarQuery($consulta)) {
            $rows = array();
            if (is_object($this->query)) {
                while ($r = $this->query->fetch_assoc()) {
                    $rows[] = $r;
                }
            }
            return $rows;
        }
        return null;
    }

    private function conectar() {
        $this->mysqli = new mysqli($this->servidor, $this->usuario, $this->contrasena, $this->base_datos);
        $this->mysqli->set_charset("utf8");
        if ($this->mysqli->connect_errno) {
            echo "Fallo al conectar a MySQL: (" . $this->mysqli->connect_errno . ") " . $this->mysqli->connect_error;
            return null;
        }
    }

    private function desconectar() {
        $this->mysqli->close();
    }

}
