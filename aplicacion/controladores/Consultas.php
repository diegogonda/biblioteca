<?php

/**
 * Description of Consultas
 *
 * @author theanswer
 */
class Consultas {

    private $baseDatos;

    public function __construct() {
        $this->baseDatos = new BaseDatos();
    }

    public function _get($args = null) {
        $query = "SELECT * FROM `consultas` WHERE 1=1";
        if (!is_null($args)) {
            $query .= " and nombre = '$args';";
        }
        $respuesta = $this->baseDatos->ejecutarQuery($query);
        return $this->limpiar($respuesta);
    }

    public function _post() {
        return [];
    }

    public function _put($nombre, $parametros) {
        if (is_string($nombre) && isset($parametros['get']) && is_array($parametros['get']) && isset($parametros['post']) && is_array($parametros['post'])) {
            $habilitada = (isset($parametros['habilitada']) && (is_numeric($parametros['habilitada'] || is_bool($parametros['habilitada'])))) ? intval($parametros['habilitada']) : true;
            $getStr = implode(';', $parametros['get']);
            $postStr = implode(';', $parametros['post']);
            $parametrosMinimos = [];
            $parametrosMinimos ['get'] = implode(';', $parametros['get']);
            $parametrosMinimos ['post'] = implode(';', $parametros['post']);
            $parametrosMinimos ['put'] = implode(';', $parametros['put']);
            $parametrosMinimos ['delete'] = implode(';', $parametros['delete']);
            
            
            $this->baseDatos->ejecutarQuery("INSERT INTO consultas (nombre, campos_minimos_get, campos_minimos_post, habilitada) VALUES ('$nombre', '$getStr','$postStr', $habilitada)
                            ON DUPLICATE KEY UPDATE nombre = '$nombre', campos_minimos_get = '$getStr', campos_minimos_post = '$postStr', habilitada = $habilitada;");
            return $this->_get($nombre);
        }
        else if (isset ($parametros['habilitada']) && boolval($parametros['habilitada'])) {
            $this->baseDatos->ejecutarQuery ("UPDATE `consultas` SET `habilitada`=1 WHERE nombre = '$nombre'"   );
        }
        return [];
    }

    public function _delete($nombre, $parametros) {
        if (is_numeric($parametros)) {
            $this->baseDatos->ejecutarQuery ("UPDATE `consultas` SET `habilitada`=0 WHERE id = " . $parametros);
        }
        return [];
    }

    public function limpiar($respuesta) {
        foreach ($respuesta as $clave => $valor) {
            $valor['id'] = intval($valor['id']);
            $valor['campos_minimos_get'] = $this->campos2array($valor['campos_minimos_get']);
            $valor['campos_minimos_post'] = $this->campos2array($valor['campos_minimos_post']);

            $respuesta[$clave] = $valor;
        }
        return $respuesta;
    }

    public function campos2array($campos) {
        $temp = explode(';', $campos);
        $array = [];
        $contador = 0;
        foreach ($temp as $t) {
            if (strcmp('', $t) !== 0) {
                $array[] = array('id' => ++$contador, 'nombre' => $t);
            }
        }
        return $array;
    }

}
