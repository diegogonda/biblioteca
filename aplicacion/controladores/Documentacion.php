<?php

/**
 * Description of Documentacion
 *
 * @author theanswer
 */
class Documentacion {

    private $tabla;

    public function __construct($informe = 'Documentacion') {
        $this->tabla = 'datos';
        $this->baseDatos = new BaseDatos();
    }

    public function _get($categoria, $parametros) {
        if (($minimos = $this->isConsulta($categoria, '_get'))) {
            $datos = [];
            $query = "SELECT datos FROM datos WHERE $this->tabla->'$.categoria' = '$categoria'";
            if (count($parametros) === 0 && $this->_isset($minimos, '-')) {
                $datos = $this->baseDatos->ejecutarQuery($query);
            } else if (count($parametros) === 1 && $this->_isset($minimos, 'id') && is_numeric($parametros)) {
                $query .= " and $this->tabla->'$.id'= $parametros;";
                $datos = $this->baseDatos->ejecutarQuery($query);
            }
            $respuesta = [];
            foreach ($datos as $dato) {
                $temp = $dato['datos'];
                $respuesta [] = json_decode($temp);
            }
            return $respuesta;
        }
        return false;
    }

    public function _post($categoria, $parametros) {
        if (($minimos = $this->isConsulta($categoria))) {
            $parametros = $this->insertarId($categoria, $parametros);
            $parametros ['categoria'] = $categoria;
            if ($this->isParametrosMinimos($minimos, $parametros)) {
                $this->baseDatos->ejecutarQuery("insert into $this->tabla (datos) values ('" . json_encode($parametros) . "')");
                return $parametros;
            }
        }
        return false;
    }

    /**
     * Comprobamos que la categoría esté dada de alta o habilitada como consulta válida.
     * @param type $categoriaStr
     * @return mixed {boolean (false) si no está permitida la consulta, los campos mínimos que tiene que tener la consulta en caso de estar permitida
     */
    public function isConsulta($categoriaStr, $metodo = '_post') {
        if (is_string($categoriaStr)) {
            $categoria = $this->baseDatos->ejecutarQuery("select * from consultas where nombre = lcase('$categoriaStr') and habilitada = 1;");
            if (is_array($categoria) && count($categoria) === 1) {
                $minimos = explode(';', trim($categoria[0]['campos_minimos' . $metodo]));
                foreach ($minimos as $clave => $valor) {
                    if (strlen($valor) === 0) {
                        unset($minimos[$clave]);
                    }
                }
                return $minimos;
            }
        }
        return false;
    }

    public function isParametrosMinimos($minimos, $parametros) {
        $temp = (object) $parametros;
        foreach ($minimos as $campo) {
            if (!isset($temp->$campo)) {
                return false;
            }
        }
        return true;
    }

    public function insertarId($consulta, $parametros) {
        $id = intval($this->baseDatos->ejecutarQuery("SELECT count(id)+1 as id FROM `datos` where datos->'$.categoria' = '$consulta';")[0]['id']);
        if (is_object($parametros)) {
            $parametros->id = $id;
        }if (is_array($parametros)) {
            $parametros['id'] = $id;
        }
        return $parametros;
    }

    public function _isset($datos, $indice) {
        if (is_string($indice)) {
            foreach ($datos as $clave => $valor) {
                if (is_string($valor) && strcmp($valor, $indice) === 0) {
                    return true;
                }
            }
        }
        return false;
    }

}
