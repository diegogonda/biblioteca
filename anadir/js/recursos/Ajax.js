var Ajax = function () {
    this.id;
    this.method;
    this.url = 'ws.php/';
};
Ajax.prototype.get = function (parametros) {
    console.log(parametros);
    this.method = 'GET';
    return this.conexion(parametros);
};
Ajax.prototype.post = function (parametros) {
    this.method = 'POST';
    return this.conexion(parametros);
};
Ajax.prototype.put = function (parametros) {
    this.method = 'PUT';
    return this.conexion(parametros);
};
Ajax.prototype.delete = function (parametros) {
    this.method = 'DELETE';
    return this.conexion(parametros);
};
Ajax.prototype.conexion = function (parametros) {
    console.log(parametros);
    console.log(parametros.data);
    return jQuery.ajax({
        method: this.method,
        context: this,
        data: parametros.data,
        url: this.url + parametros.ruta,
        timeout: 30000, // sets timeout to 30 seconds
        headers: parametros.headers,
        beforeSend: function (xhr) {

        },
        success: function (data, textStatus, jqXHR) {

        },
        error: function (httpObj, textStatus) {
            if (httpObj.status === 401) {

            } else if (httpObj.status === 409) {
                $('#dialog').alertas('Error 409', JSON.parse(httpObj.responseText).resultado);
            } else if (httpObj.status === 503) {
                $('#dialog').alertas('Error 503', JSON.parse(httpObj.responseText).resultado);
            }
        }
    });
};
