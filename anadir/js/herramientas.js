/**
 * Automatización de la construcción de código HTML de forma recursiva
 * Recibe un objeto JSON con la estructura de la división y todo su contenido en forma de pares clave/valor
 * Así, todas las divisiones tendrán un indice, y dicho indice contendrá los valores de la división de la 
 * forma que sigue:
 * var div = {
 *      indice_x: {
 *          id : 'id_',
 *          etiqueta: '',
 *          atributos {
 *              class: '',
 *              xxx: 'xxx'
 *          }, 
 *          cierre : 'opcion_cierre',
 *          texto: {'string' || number || object}
 *      }
 * }
 * 
 * NOTAS:
 *      1.- etiqueta (html) puede ser cualquier etiqueta html : div, span, input...
 *      2.- cierre : Determina como se cerrara una etiqueta. Opciones:
 *          2.1.- completo: Se crea una etiqueta con inicio y final (por ejemplo: <div></div>)
 *          2.2.- solo_inicio: solo se crea la etiqueta de inicio: (por ejemplo: <div>)
 *          2.3.- solo_cierre: solo se crea la etiqueta de cierre: (por ejemplo: </div>)
 *          2.4.- en_linea: para etiquetas que no tienen un cierre propiamente dicho: (por ejemplo: <input ... />)
 *      3.- texto: Como se indica en el esquema, texto puede ser un string o contener un objeto con más contenidos, 
 *          que serán agregados de forma recursiva
 *      4.- Indice puede ser de tipo numérico o de tipo string
 * @param {object} contenido Objeto JSON
 * @returns {String} Versión HTML del JSON recibido
 */
function json2html(contenido) {
    var append = '';
    for (var i in contenido) {
        if (typeof contenido[i].cierre === 'undefined') {
            contenido[i].cierre = 'completo';
        }
        if (contenido[i].cierre === 'completo' || contenido[i].cierre === 'solo_inicio' || contenido[i].cierre === 'en_linea') {
            append += '<' + contenido[i].etiqueta + ' ' + getAtributos(contenido[i].atributos);
            if (contenido[i].cierre !== 'en_linea') {
                append += '>';
            }
        }
        if ((typeof contenido[i].texto === 'string' || typeof contenido[i].texto === 'number') && contenido[i].cierre !== 'en_linea') {
            append += contenido[i].texto;
        } else if (typeof contenido[i].texto === 'object') {
            append += json2html(contenido[i].texto);
        }
        if (contenido[i].cierre === 'completo' || contenido[i].cierre === 'solo_cierre') {
            append += '</' + contenido[i].etiqueta + '>';
        } else if (contenido[i].cierre === 'en_linea') {
            append += '/>';
        }
    }
    return append;
}
function getAtributos(atributos) {
    var append = '';
    for (var i in atributos) {
        var attr = i.replace('_', '-');
        append += attr + '="' + atributos [i] + '" ';
    }
    return append;
}