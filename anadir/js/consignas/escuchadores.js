/* global ajax */

$.fn.escuchadoresConsignas = function () {
    $('body').on('click', '.btn', function (e) {
        if (this.id === 'submit-form') {
            var url = $('#input-url').val();
            var titulo = $('#input-titulo').val();
            var contenido = $('#input-contenido').val();

            console.log(url);
            console.log(titulo);
            console.log(contenido);

            var urlPattern = /(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/
            if (urlPattern.test(url)) {
                console.log('es url');
            }
        }
        return false;
    }).on('click', '.menu, .btn', function (e) {
        if (this.id === 'consignas') {
            ajax.getConsignas().done(function (respuesta) {
                $('#contenido').imprimirConsignas(respuesta.contenido);
                $('#contenido').insertarInputNuevaConsigna();
            });
        } else if (this.id === 'modificar-consignas') {
            var id = $(this).data('id');
            var $span = $('span span-consignas').data(id);
            var texto = $span.text();
            var input = {
                input: {
                    etiqueta: 'input',
                    cierre: 'completo',
                    atributos: {
                        value: texto
                    }
                }
            };
            console.log(json2html(input));
            $span.empty().append(json2html(input));
        }
    }).on('keypress', function (e){
        if ( e.which === 13 ) {
//            alert();
        }
    });
};

