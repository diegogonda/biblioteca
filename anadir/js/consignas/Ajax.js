/* global Ajax, rutas */

Ajax.prototype.getConsignas = function (id) {
    var ruta = rutas['consignas'];
    if (id) {
        ruta += '/' + id;
    }
    return this.get({ruta: ruta});
};