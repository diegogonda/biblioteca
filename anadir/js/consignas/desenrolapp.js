$.fn.imprimirConsignas = function (consignas) {
    if (consignas) {
        $('#contenido').empty();
        consignas.forEach(function (v, i) {
            var json = {consignas: {
                    etiqueta: 'div',
                    texto: {
                        texto: {
                            etiqueta: 'span',
                            atributos: {
                                class: 'span-consigna',
                                data_id: i
                            },
                            texto: v.consigna
                        },
                        modificar: {
                            etiqueta: 'button',
                            atributos: {
                                class: 'btn btn-primary btn-xs'
                            },
                            texto: {
                                span: {
                                    etiqueta: 'span',
                                    atributos: {
                                        class: 'glyphicon glyphicon-pencil',
                                        id: 'modificar-consignas',
                                        data_id: i
                                    }
                                }
                            }
                        },
                        eliminar: {
                            etiqueta: 'button',
                            atributos: {
                                class: 'btn btn-danger btn-xs'
                            },
                            texto: {
                                span: {
                                    etiqueta: 'span',
                                    atributos: {
                                        class: 'glyphicon glyphicon-remove',
                                        id: 'eliminar-consignas',
                                        data_id: i

                                    }
                                }
                            }
                        }

                    }
                }
            };
            console.log(json2html(json));
            $('#contenido').append(json2html(json));
        });
    }
};
$.fn.insertarInputNuevaConsigna = function (){
    var input = {
        input: {
            etiqueta: 'input',
            cierre: 'en_linea',
            atributos: {
                class: 'form form-control',
                id: 'nueva-consigna',
                type: 'text'
            }
        }
    };
    $(this).append (json2html(input));
};