
$.fn.escuchadoresConsultas = function () {
    $(this).on('keypress', '#input-consulta-campos', function (e) {
        var campo = $(this).val();
        var patron = /[A-Za-z]/;
        if (e.which === 13 && patron.test(campo)) {
            var contenido = $(this).attr('data-contenido');
            contenido += campo + ';'
            $(this).attr('data-contenido', contenido);
            var split = contenido.split(';');
            $('#span-consulta-campos').empty();
            for (var i in split) {
                $('#span-consulta-campos').append(json2html({
                    texto: {
                        etiqueta: 'div',
                        texto: split[i]
                    }

                }));
            }
            $(this).val('');
        }
    });
};
