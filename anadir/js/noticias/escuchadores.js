/* global ajax */

$.fn.escuchadoresNoticias = function () {
    $(this).on('click', '.btn', function (e) {
        e.preventDefault();
        if (this.id === 'button-enviar-noticia') {
            var datos = {
                url: $('#input-url').val(),
                categoria: $('#input-categoria').val(),
                nota: $('#input-nota').val()
            };
            ajax.postNoticia(datos);
        }
        return false;
    });
};